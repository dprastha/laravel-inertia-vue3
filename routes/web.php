<?php

use Illuminate\Support\Facades\Route;

use App\Models\Produk;
use App\Http\Resources\ProductCollection;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', [\App\Http\Controllers\ProductController::class, 'index']);
// Route::get('/productlists', [\App\Http\Controllers\ProductController::class, 'indexlist']);
// Route::resource('/products', \App\Http\Controllers\ProductController::class);
// Route::resource('/categories', \App\Http\Controllers\CategoryController::class);

// Route::get('/statelist', [\App\Http\Controllers\CityController::class, 'StateList']);
// Route::get('/citylists', [\App\Http\Controllers\CityController::class, 'indexlist']);


Route::get('/', [\App\Http\Controllers\CityController::class, 'index']);

Route::resource('/cities', \App\Http\Controllers\CityController::class);
Route::resource('/states', \App\Http\Controllers\StateController::class);


