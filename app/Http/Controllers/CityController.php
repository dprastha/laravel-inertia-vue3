<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Models\City;
use App\Models\State;
use App\Http\Resources\CityCollection;
use Inertia\Inertia;

class CityController extends Controller
{
    public function index()
    {
        $city = City::with('CityState')->get();
        return Inertia::render('Kota/Index', 
        [
            'city' => $city
        ]);
    }
    public function create()
    {
        $state = State::all();
        return Inertia::render('Kota/Create', 
        [
            'state' => $state
        ]);
    } 
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        $city = City::create([
            'provinsi_id'  => $request->provinsi_id,
            'namakota'     => $request->namakota,
        ]);

        if($city) {
            return Redirect::route('cities.index')->with('message', 'Data Berhasil Disimpan!');
        }
    }
    public function edit(City $city)
    {
        $state = State::all();
        return Inertia::render('Kota/Edit', [
            'city' => $city,
            'state' => $state
        ]);
    }
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, City $city)
    {
        $request->validate([
            'provinsi_id' => 'required',
            'namakota'    => 'required'
        ]);
        $city->update([
            'provinsi_id' => $request->provinsi_id,
            'namakota'    => $request->namakota
        ]);
        if($city) {
            return Redirect::route('cities.index')->with('message', 'Data Berhasil Diupdate!');
        }
    }
    public function destroy($id)
    {
        //find post by ID
        $city = City::findOrfail($id);
        //delete post
        $city->delete();
        if($city) {
            return Redirect::route('cities.index')->with('message', 'Data Berhasil Dihapus!');
        }
    }

    public function indexlist()
    {
        return new CityCollection(City::all());
    }
    public function StateList()
    {
        $data = City::get();
        return response()->json($data);
    }
}
