<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Models\Produk;
use App\Models\Kategori;
use Inertia\Inertia;

class CategoryController extends Controller
{
    public function index()
    {
        //get all posts from database
        $category = Kategori::all();

        //render with data "posts"
        return Inertia::render('Kategori/Index', [
            'category' => $category
        ]);
    }
    public function create()
    {
        return Inertia::render('Kategori/Create');
    } 
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //create post
        $category = Kategori::create([
            'namakategori'     => $request->namakategori,
        ]);

        if($category) {
            return Redirect::route('categories.index')->with('message', 'Data Berhasil Disimpan!');
        }
    }
    public function edit(Kategori $category)
    {
        return Inertia::render('Kategori/Edit', [
            'category' => $category
        ]);
    }
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, Kategori $category)
    {
        //set validation
        $request->validate([
            'namakategori'   => 'required'
        ]);

        //update post
        $category->update([
            'namakategori' => $request->namakategori
        ]);

        if($category) {
            return Redirect::route('categories.index')->with('message', 'Data Berhasil Diupdate!');
        }
    }
    public function destroy($id)
    {
        //find post by ID
        $category = Kategori::findOrfail($id);

        //delete post
        $category->delete();

        if($category) {
            return Redirect::route('categories.index')->with('message', 'Data Berhasil Dihapus!');
        }

    }
}
