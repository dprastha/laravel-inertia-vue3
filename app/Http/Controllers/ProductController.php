<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Models\Produk;
use App\Models\Kategori;
use App\Http\Resources\ProductCollection;
use Inertia\Inertia;

class ProductController extends Controller
{
    public function index()
    {
        //get all posts from database
        $produk = Produk::all();

        //render with data "posts"
        return Inertia::render('Produk/Index', [
            'produk' => $produk
        ]);
    }
    public function create()
    {
        return Inertia::render('Produk/Create');
    } 
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //create post
        $product = Produk::create([
            'namaproduk'     => $request->namaproduk,
        ]);

        if($product) {
            return Redirect::route('products.index')->with('message', 'Data Berhasil Disimpan!');
        }
    }
    public function edit(Produk $product)
    {
        return Inertia::render('Produk/Edit', [
            'product' => $product
        ]);
    }
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, Produk $product)
    {
        //set validation
        $request->validate([
            'namaproduk'   => 'required'
        ]);

        //update post
        $product->update([
            'namaproduk' => $request->namaproduk
        ]);

        if($product) {
            return Redirect::route('products.index')->with('message', 'Data Berhasil Diupdate!');
        }
    }
    public function destroy($id)
    {
        //find post by ID
        $product = Produk::findOrfail($id);

        //delete post
        $product->delete();

        if($product) {
            return Redirect::route('products.index')->with('message', 'Data Berhasil Dihapus!');
        }

    }

    public function indexlist()
    {
        return new ProductCollection(Produk::all());
    }
}
