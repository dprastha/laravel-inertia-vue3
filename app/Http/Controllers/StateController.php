<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Models\City;
use App\Models\State;
use App\Http\Resources\StateCollection;
use Inertia\Inertia;


class StateController extends Controller
{
    public function index()
    {
        $state = State::all();
        return Inertia::render('Provinsi/Index', [
            'state' => $state
        ]);
    }
    public function create()
    {
        return Inertia::render('Provinsi/Create');
    } 
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //create post
        $state = State::create([
            'namaprovinsi' => $request->namaprovinsi,
        ]);

        if($state) {
            return Redirect::route('states.index')->with('message', 'Data Berhasil Disimpan!');
        }
    }
    public function edit(State $state)
    {
        return Inertia::render('Provinsi/Edit', [
            'state' => $state
        ]);
    }
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, State $state)
    {
        //set validation
        $request->validate([
            'namaprovinsi'   => 'required'
        ]);

        //update post
        $state->update([
            'namaprovinsi' => $request->namaprovinsi
        ]);

        if($state) {
            return Redirect::route('states.index')->with('message', 'Data Berhasil Diupdate!');
        }
    }
    public function destroy($id)
    {
        //find post by ID
        $state = State::findOrfail($id);

        //delete post
        $state->delete();

        if($state) {
            return Redirect::route('states.index')->with('message', 'Data Berhasil Dihapus!');
        }

    }
}
